<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/20
 * Time: 17:53
 */
//设置默认时区，防止异常
date_default_timezone_set('UTC');
header('content-type:text/html;charset=utf-8');
echo '时间戳为：'.time();
echo '<hr/>';
echo date('Y-m-d H:i:s'),'<br/>';
echo '一天之后的这个时间',date('Y-m-d H:i:s', time()+24*60*60);
echo '<hr/>';
echo mktime(0, 0, 0, 6, 1, 1994);
echo '</br>';
echo date('Y-m-d H:i:s', mktime(0, 0, 0, 6, 1, 1990));
echo '</hr>';
echo '</br>';
$birth = mktime(0, 0, 0, 6,1 , 1990);
$time = time();
$age = floor(($time - $birth)/(24*3600*365));
echo '活了多少岁：',$age;

echo '</br>';
echo '计算今年与94年的年距:';
$distant = date('Y')-1994;
echo $distant;

echo '</br>';
echo strtotime("last Monday"), "\n";

echo '</br>';
echo time();
echo strtotime('now'),'<br/>';

echo '</br>';
echo date('Y-m-d H:i:s', time()+24*3600),'</br>';
echo date('Y-m-d H:i:s', strtotime('+1 day')),'</br>';
echo date('Y-m-d H:i:s', strtotime('-1 day'));
echo date('Y-m-d H:i:s', strtotime('+5 days')),'</br>';
echo date('Y-m-d H:i:s', strtotime('+1 month')),'</br>';

echo '</br>';
echo 'microtime返回微秒';
echo '</br>';
echo  microtime(),'</br>';
echo time(),'</br>';
echo microtime(true),'</br>';
$start = microtime(true);
for($i=0; $i<10000; $i++) {
    $array[] = $i;
}
$end = microtime(true);
echo $end - $start,'</br>';
echo round($end - $start ,4),'</br>','</br>';

print_r(getdate());
echo '</br>';
print_r(gettimeofday());
echo '</br>';
var_dump(checkdate(8, 12, 2016));
echo '</br>';
var_dump(checkdate(18, 12, 2016));
echo '</br>';
echo '</br>';
