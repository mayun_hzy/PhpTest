<?php
header('content-type:text/html;charset=utf-8');
$mysqli = new mysqli('localhost', 'root', 'root', 'imooc_mysqli');
if ($mysqli->connect_errno) {
    die('连接错误'.$mysqli->connect_error);
}
$sql = "Select id, username, password FROM user ";
$mysqli_result = $mysqli->query($sql);
if ($mysqli_result && $mysqli_result->num_rows) {
    while ($assoc = $mysqli_result->fetch_assoc()) {
        //将查询到到每一条记录放在索引数组中
        $rows[] = $assoc;
    }
}
print_r($rows);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>用户列表</title>
</head>
<body>
<h2>用户列表--<a href='addUser.php'>添加用户</a></h2>
<table border="1" cellpadding='0' cellspacing='0' width="80%" bgcolor="aqua" >
    <tr>
        <td>编号</td>
        <td>用户名</td>
        <td>年龄</td>
        <td>操作</td>
    </tr>
    <?php $i=1; foreach ($rows as $row):?>
        <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $row['username'];?></td>
            <td><?php echo $row['password'];?></td>
            <td><a href='updateUser.php?act=updateUser&id=<?php echo $row['id'];?>'>更新</a>|<a href="doAction.php?act=removeUser&id=<?php echo $row['id'];?>">删除</a></td>
        </tr>
    <?php $i++; endforeach;?>
</table>
</body>
</html>