<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/24
 * Time: 09:15
 */
header('content-type:text/html;charset=utf-8');
//1、建立到MySQL到连接并打开数据库
$mysqli = @new mysqli('localhost', 'root','root', 'imooc_mysqli');
if ($mysqli->connect_errno) {
    die('错误信息为： '.$mysqli->connect_error);
}

//2、设置默认编码方式为utf8
$mysqli->set_charset('utf8');

//3、执行SQL查询
$sql =<<<EOF
     CREATE TABLE IF NOT EXISTS mysqli(
          id TINYINT UNSIGNED AUTO_INCREMENT KEY,
          username VARCHAR(20) NOT NULL
     );  
EOF;
$res = $mysqli->query($sql);
var_dump($res);
/*
 * SELECT/DESC/DESCRIBE/SHOW/EXPLAIN/执行成功返回mysql_result对象
 * ，执行失败返回false
 * 对于其他SQL语句的执行，成功返回true,失败返回false
 */

//关闭连接
$mysqli->close();