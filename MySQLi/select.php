<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/25
 * Time: 11:12
 */
header('content-type:text/html;charset=utf-8');
$hzy = new mysqli('localhost', 'root', 'root', 'imooc_mysqli');
if ($hzy->connect_errno) {
    die('数据库错误: ' . $hzy->connect_error);
}
$hzy->set_charset('utf8');
//$sql = "SELECT * FROM user1";
$sql = "SELECT * FROM user";
//返回结果集对象
$result = $hzy->query($sql);
if ($result && $result->num_rows > 0) {
    print_r($result);
    //取得结果集中记录条数
    //方法一, 获取结果集中所有记录，默认返回的是二维的索引+索引的形式
    // $rows = $result->fetch_all();
    //方法二，
    $rows = $result->fetch_all(MYSQLI_NUM);//返回索引部分
    $rows = $result->fetch_all(MYSQLI_ASSOC);//返回关联部分
    $rows = $result->fetch_all(MYSQLI_BOTH);//既有索引部分，也有关联部分
    $row = $result->fetch_row();//取得结果集中第一条记录，作为索引数组返回
    $row = $result->fetch_assoc();//取得结果集中的一条记录返回，作为关联数组返回

    print_r($row);
    $row = $result->fetch_assoc();//每fetch一次向下移动一条记录
    $row = $result->fetch_array();//二者都返回
    $row = $result->fetch_array(MYSQLI_ASSOC);//返回关联部分
    print_r($row);

    echo '<hr color="orange">';
    $row = $result->fetch_object();
    print_r($row);

    //移动结果集内部指针
    $result->data_seek(3);
    $row = $result->fetch_assoc();
    print_r($row);

    while ($row = $result->fetch_assoc()) {
        //print_r($row);
        $rows[] = $row;
        echo '<hr/>';
    }
    print_r($rows);
    //释放结果集
    $result->free();

} else {
    echo '查询错误或者结果集中没有记录';
}
$hzy->close();
echo '<hr color="orange">';