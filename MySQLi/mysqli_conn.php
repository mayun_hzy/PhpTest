<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/24
 * Time: 01:41
 */
header('content-type:text/html;charset=utf-8');
////1,建立到MySQL数据库的连接
//$mysql = new mysqli('localhost', 'root', 'root');
//print_r($mysql);
//
//echo '<hr/>';
////2,打开指定的数据库
//$mysql->select_db('imooc_mysqli');
//$mysqli = new mysqli();
//$mysqli->connect('127.0.0.1', 'root', 'root');
//print_r($mysqli);

//建立连接的同时，打开数据库
//$mysqli = new mysqli('127.0.0.1', 'root', 'root', 'imooc_mysqli');
//print_r($mysqli);

//密码错误信息, @号表示屏蔽错误提醒
//$mysqli = new mysqli('127.0.0.1', 'root', 'root123', 'imooc_mysqli');
$mysqli = @new mysqli('127.0.0.1', 'root', 'root123', 'imooc_mysqli');
//$mysqli->connect_errno:得到连接产生的错误编号
//$mysqli->connect_error:得到连接产生的错误信息
//if ($mysqli->connect_error) {
//    ///die — 等同于 exit();   exit — 输出一个消息并且退出当前脚本
//    die('连接错误:'.$mysqli->connect_error);
//}
//print_r($mysqli);


$mysqli = @new mysqli('127.0.0.1', 'root', 'root', 'imooc_mysqli');
echo '<hr color="red"/>';
echo '客户端的信息： '.$mysqli->client_info.'<br/>';
echo '客户端的信息： '.$mysqli->get_client_info().'<br>';
echo '客户端的版本：'.$mysqli->client_version.'<br>';
echo '服务器端的版本：'.$mysqli->server_info.'<br>';
echo $mysqli->get_server_info();
echo '<hr>';
echo '服务器版本： '.$mysqli->server_version;
