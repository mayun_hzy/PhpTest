<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/24
 * Time: 09:35
 */
header('content-type:text/html;charset=utf-8');
$hzy = @new mysqli('localhost', 'root', 'root', 'imooc_mysqli');
if ($hzy->connect_errno) {
    die('连接出错：'.$hzy->connect_error);
}
$hzy->set_charset('utf-8');
//执行SQL查询
//添加记录
//执行单条语句,只能执行一条SQL语句
$sql = "INSERT user(id, username) values(null,'黄朝阳')";
$boolSuccess = $hzy ->query($sql);
switch ($boolSuccess) {
    case 0 :
        echo 'ERROR'.$hzy->errno.':'.$hzy->error;
        break;
    case 1 :
        //得到上一插入操作产生的AUTO_INCREMENT的值
        echo '注册成功！'.'您是第：'.$hzy->insert_id.'位用户';
        break;
    default :
        echo '黄朝阳';
}

echo '<hr color="orange"/>';
//批量插入
$sql2 = "INSERT user(username) values('Tom'),('Mary'),('Jack'),('AOP')";
$boolSuccess = $hzy ->query($sql2);
switch ($boolSuccess) {
    case 0 :
        echo 'ERROR'.$hzy->errno.':'.$hzy->error;
        break;
    case 1 :
        //得到上一插入操作产生的AUTO_INCREMENT的值
        echo '注册成功！'.'您是第：'.$hzy->insert_id.'位用户'.'<br>';
        //得到上一步操作产生的受影响记录条数
        echo '有'.$hzy->affected_rows.'记录被影响';
        break;
    default :
        echo '黄朝阳';
}

//将表中年龄+10
$sql = "UPDATE user SET username = '名字update'";
$boolSuccess = $hzy->query($sql);
switch ($boolSuccess) {
    case 0 :
        echo '错误信息： '.$hzy->connect_errno.':'.$hzy->connect_error;
        break;
    case 1 :
        echo '有'.$hzy->affected_rows.'条记录更新';
        break;
    default:
        echo 'default';
}
echo '<hr color="orange">';

echo '</br>';
//将表中id>=6的用户删掉
$sql = "DELETE FROM user WHERE id >=6 ";
$res = $hzy->query($sql);
if ($res) {
    echo $hzy->affected_rows.'条记录被删除';
}
switch ($res) {
    case 0 :
        die('数据库错误： '.$hzy->error);
        break;
    case 1 :
        echo $hzy->affected_rows.'条记录被删除';
        break;
    default :
        echo 'huangzhaoyang';
}
//关闭到MySQL到连接
$hzy->close();