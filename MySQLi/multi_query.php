<?php
/**
 * Created by PhpStorm.
 * User: Huang Zhaoyang
 * Date: 2019/8/9
 * Time: 16:20
 */
header('content-type:text/html;charset=utf-8');
$mysqli = new mysqli('localhost', 'root', 'root', 'imooc_mysqli');
$mysqli->set_charset('utf8');
if ($mysqli->connect_errno) {
    die($mysqli->connect_error);
}
//每条sql语句用;号结尾
$sql = "INSERT user(username, password) VALUES('imooc', 'imooc');";
$sql.= "UPDATE user SET password = 'imooc password  had changed' WHERE id = 68;";
$sql.="DELETE FROM user WHERE id = 4;";
//执行单条
$mysqli_result = $mysqli->query($sql);
if ($mysqli_result) {
    echo '插入成功';
} else {
    echo '插入失败';
}
//mysqli 执行多条语句，使用multi_query可以查询多个表记录，得到多个结果集
$multi_query = $mysqli->multi_query($sql);
var_dump($multi_query);