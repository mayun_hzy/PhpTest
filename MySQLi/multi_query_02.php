<?php
/**
 * Created by PhpStorm.
 * User: Huang Zhaoyang
 * Date: 2019/8/9
 * Time: 17:04
 */
header('content-type:text/html;charset=utf-8');
$mysqli = new mysqli('localhost', 'root', 'root', 'imooc_mysqli');
$mysqli->set_charset('utf8');
if ($mysqli->connect_errno) {
    die($mysqli->connect_error);
}
$sql = "SELECT * FROM user;";
$sql.="SELECT * FROM mysql.user;";
//use_result() /store_result()
//使用store_result()得到的结果集效率很高
//more_results(): 检测是否有更多的结果集
//next_result():将结果集指针向下移动一位，结果为布尔类型
if ($mysqli->multi_query($sql)) {
    do {
        if ($mysqli_result = $mysqli->store_result()) {
            $mysqli_result->fetch_all(MYSQLI_ASSOC);
        }
    } while(s);
}

