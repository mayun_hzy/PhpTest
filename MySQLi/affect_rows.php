<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/25
 * Time: 11:12
 */
header('content-type:text/html;charset=utf-8');
$hzy = new mysqli('localhost', 'root', 'root', 'imooc_mysqli');
if ($hzy->connect_errno) {
    die('数据库错误: '.$hzy->connect_error);
}
$hzy->set_charset('utf8');
//$sql = "INSERT user(username1) VALUES('A')";
$hzy->query($sql);
echo $hzy->affected_rows;

echo '<hr color="orange">';
$sql = "DELETE FROM user WHERE id=1";
$hzy->query($sql);
echo $hzy->affected_rows;
$hzy->close();

/*
 * affected_rows值为3种:
 * 1,受影响的记录条数
 * 2,-1，代表SQL语句有问题
 * 3, 0，代表没有受影响记录的条数
 * */
echo '<hr color="orange">';