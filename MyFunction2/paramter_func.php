<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/23
 * Time: 12:50
 */
header('content-type:text/html;charset=utf-8');
$arr = array('a', 'b', 'c');
//array
array_pop($arr);
var_dump($arr);

/*传值：默认情况下，函数参数通过值传递
,所以即使在函数内部改变参数的值也不会改变函数外部的值
*/
/*
 * 传引用，可以通过在参数前添加&符号，代表通过引用传递参数
 * ，在函数内部对其所操作影响其本身
 */
//只有变量能当作引用被传递
echo '</br>';
//取变量的地址
function test1(&$j) {
    $j += 13;
    var_dump($j);//int(18)
}
$m = 5;
test1($m);
var_dump($m); //int(18)

/*get_defined_functions()得到所有已定义的函数，返回是数组
，包含系统函数和用户自定义的函数
*/
//这个函数非常有用，特别用于二次开发
print_r(get_defined_functions());