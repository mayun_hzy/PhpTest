<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/23
 * Time: 11:41
 */
header('content-type:text/html;charset=utf-8');
$i=1;
$j=2;
function test1() {
    var_dump($i, $j);
}

echo '</br>';
function test2() {
    global $i, $j;
    var_dump($i, $j);
    $i = 3;
    $j = 5;
}
test2();
echo '</br>';
var_dump($i, $j);
function test3() {
    //global $m = 2;
}
test3();

echo '</br>';
//global声明变量的正确方式
function test4() {
    global $m,$n;
    $m = 25;
    $n = 35;
    echo '\$m: ',$m,'\$n: ',$n;
}
test4();

echo '<hr>';

$username = 'king';
$age = '15';
$email = '123@qq.com';

//超全局变量，不管函数体内，函数体外都能得到
print_r($GLOBALS);
echo '</br>';
function test5() {
    echo '用户名为： '.$GLOBALS['username'].'</br>';
    echo '年龄为： '.$GLOBALS['age'].'</br>';
    echo '邮箱为： '.$GLOBALS['email'].'</br>';
}
test5();

echo '</br>';
//超全局变量赋值
function test6() {
    $GLOBALS['age'] = 22;
}
test6();
var_dump($age);