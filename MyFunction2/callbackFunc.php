<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/23
 * Time: 13:20
 */
header('content-type:text/html;charset=utf-8');
function study() {
    echo 'studying...','</br>';
}
function play() {
    echo 'play...','</br>';
}
function sing() {
    echo 'sing...','</br>';
}

function doWhat($funName) {
    echo '我正在';
    //通过可变函数的形式进行调用
    $funName();
}

doWhat('study');