<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/23
 * Time: 08:33
 */
header('content-type:text/html;charset=utf-8');
//检测函数名称是否存在的函数
var_dump(function_exists('strtolower'));
var_dump(function_exists('hzy'));
function test1() {
    return md5('hzy');
}
echo test1();

echo '</br>';
/**
 * 创建3行2列的表格
 * 按照需求创建指定的表格
 * @param number $rows
 * @param number $cols
 * @return string
 */
function createTable($rows, $cols) {
    $table = "<table border='1' cellpadding='0' cellspacing='0' width='80%'>";
    for($i = 0; $i<$rows; $i++) {
        $table .= '<tr>';
        for($j=0; $j<$cols; $j++){
            $table .= '<td>hzy</td>';
        }
        $table .= '</tr>';
    }
    $table.= "</table>";
    return $table;
}
echo createTable(5, 6);
echo '</br>';
echo '<hr>';

function createMyTable($rows=3, $cols=5, $color='red', $content='hzy') {
    $table = "<table border='1'  bgcolor='{$color}' cellspacing='0' cellpadding='0' width='80%'>";
    for($i =0 ;$i<$rows;$i++) {
        $table = '<tr>';
        for($j=0; $j<$cols;$j++){
            $table .= "<td>{$content}</td>";
        }
        $table .='</tr>';
    }
    $table.= "</table>";
    return $table;
}

echo createMyTable(3,5,'pink');
echo '</br>';

//获得文件拓展名
if (!function_exists('getFileExtend')) {
    function getFileExtend($fileName){
        return pathinfo($fileName, PATHINFO_EXTENSION);
    }
}
echo getFileExtend("123hzy.php");
echo '</br>';

//print_r(get_defined_functions());