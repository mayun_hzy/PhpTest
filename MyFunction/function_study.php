<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/21
 * Time: 17:28
 */
header('content-type:text/html;charset=utf-8');

$array = array(3, 55, 25, 6, 10, 2, 0);
$max = $array[0];
foreach ($array as $item){
    if ($item>$max) {
        $max = $item;
    }
};
echo '最大值为：',$max,'</br>';

echo '<hr>';
function get_int() {
    return 2*23;
}
function get_float() {
    return 3.14;
}
function get_string() {
    return 'hello';
}
function get_array() {
    return array(2, 3, 4, 5);
}
echo get_int();
echo '</br>';
echo get_float();
echo '</br>';
echo get_string();
echo '</br>';
echo print_r(get_array());

echo '</br>';
function get_max_value ($myArray){
    $max = $myArray[0];
    foreach ($myArray as $item){
        if ($item>$max) {
            $max = $item;
        }
    };
    return $max;
}
$myArray = array(2, 43, 45, 11, 8, 9);
echo get_max_value($myArray),'</br>';

function get_args($hzy, $fo) {
    echo $hzy,'和 ',$fo;
}
get_args(1);
echo '</br>';
get_args(1, 2);
echo '</br>';
get_args(1, 2, 3);
echo '</br>';
get_args(4-2, 5-3);
echo '</br>';
$a1 = 30;
$b1 = 20;
get_args($a1+$b1, 45.67);
echo '</br>';
echo '<hr>';


function greet_to_someont($name, $is_formal=false){
    if (!$is_formal) {
        echo "Hi,",$name,"</br>";
    } else {
        echo "Hello,",$name,"</br>";
    }
}
greet_to_someont("Mary");
greet_to_someont("Tom", true);

function sum($a, $b) {
    return $a + $b;
}

echo '</br>';
function get_num() {
    $args_num = func_num_args();//获得传入的参数的个数
    $num = 0;
    if ($args_num == 0) {
        return $num;
    } else {
        for($i=0; $i<$args_num; $i++) {
            $item = func_get_arg($i);//获得某一项参数的值
            $num += $item;
        }
        return $num;
    }
}
echo get_num(),'</br>';
echo get_num(2, 3),"</br>";

echo "值传递&引用传递",'</br>';
function factorial($num) {
    $result = 1;
    for($i=1; $i<=$num; $i++) {
        $result = $result*$i;
    }
    $num = 5;//测试形参是否会改变实参
    return $result;
}
echo factorial(5);
echo '</br>',"测试值是否改变","</br>";
$num1 = 3;
echo factorial($num1);
echo '</br>';
echo '$num1 : ',$num1;
echo '结论：在内存中，实参单元和形参单元是不同的单元，在调用函数时
，它会给形参分配一个存储空间，并将实参对应的值传递给形参
，调用结束之后，形参单元会被释放；实参单元保持并且维持原来的值；
故形参的值改变不会影响实参的值';
echo "</br>";

echo '<hr>';
echo '值传递&引用传递','</br>';

//没有返回值
function swap(&$a, &$b){
    $temp = $a;
    $a = $b;
    $b = $temp;
}
$sa = 3;
$sb = 5;
echo '调用前：',"sa:",$sa," ","sb:",$sb;
echo '</br>';
swap($sa, $sb);
echo '调用后：',"sa:",$sa," ","sb: ",$sb;
echo '</br>';
echo '引用传递并没有开辟新的内存空间,所以在swap函数中的操作直接影响了原来的值';

echo '<hr>';
echo '变量的作用域','</br>';