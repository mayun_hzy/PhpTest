<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/22
 * Time: 16:17
 */
header('content-type:text/html;charset=utf-8');
echo '可变函数';
echo '</br>';
echo '变量名后有圆括号，PHP将寻找与变量的值同名的函数，并且尝试执行它';
echo '</br>';
echo 'echo() print() unset() isset() empty() include require()是语言结构
,不能直接用于可变函数';
echo '</br>';
function get_apple($num)
{
    return "in get_apple " . $num;
}

function get_orange($num)
{
    return "in get_orange" . $num;
}

function print_str($str)
{
    echo $str, "\n";
}

function get_fruit($fruit, $num)
{
    $opt = 'get_' . $fruit;
    return $opt($num);
}

$f1 = 'get_apple';
echo $f1(7), '</br>';
$f1 = 'get_orange';
echo $f1;
echo '</br>';
echo get_fruit('apple', 5);

echo '</br>';
function get_fruit2($fruit, $num){
    $res = '';
    if ($fruit == "apple") {
        $res = get_apple($num);
    } else if ($fruit == 'orange') {
        $res = get_orange($num);
    }
    return $res;
}
echo get_fruit2('apple', 25);