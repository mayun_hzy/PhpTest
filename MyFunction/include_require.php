<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/22
 * Time: 18:31
 */
header('content-type:text/html;charset=utf-8');

include ('out_in.php');//include和require一样
require ('say_hello.php');//不可以放在最后面
echo '如果我们通过include引入一个不存在的文件时，只会产生一个警告
；而require会导致一个致命的错误。换句话说，如果想在丢失文件的时候
，停止处理页面就用require，如果你想这个错误可以被忽略掉
，使程序继续运行下去，就用include；';
say_hello('Tom');
echo '程序结束';
echo '</br>';
echo 'require_once和include_once只会抛出警告，而不会出现致命错误';
echo '</br>';

echo '路径引用, set_include_path和get_include_path','</br>';


