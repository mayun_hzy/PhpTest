<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/22
 * Time: 17:06
 */
header('content-type:text/html;charset=utf-8');
function out() {
    echo '我是out函数';
    function in() {
        echo '如果外部函数out()没有被调用的话，我是不存在的';
    }
}
//in();
out();
echo '</br>';
in();
//out();//不能对in进行重复定义
function out2() {
    if (!function_exists('in2')) {
        echo '我是out函数';
        function in2() {
            echo '如果外部函数out()没有被调用的话，我是不存在的';
        }
    }
}
//in();
out2();
echo '</br>';
in2();
out2();//能对out2重复调用

echo '</br>';
echo '多层嵌套'.'</br>';
function f_out() {
    echo 'f_out '.'</br>';
    function f_mid() {
        echo 'f_mid'.'</br>';
        function f_tail() {
            echo 'f_tail'.'</br>';
        }
    }
}
f_out();
f_mid();
f_tail();