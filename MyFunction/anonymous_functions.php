<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/22
 * Time: 17:56
 */
header('content-type:text/html;charset=utf-8');
//这里相当于把函数名仍到前面，很像可变函数
$message = "hello";
$example = function () use($message){
//  echo 'hello';
    echo $message;
};
$message = "Hi";
$example();

echo '</br>'.'使用引用传递改变变量值','</br>';
$message2 = "hello";
$example2 = function ($name) use(&$message2){
//  echo 'hello';
    echo $message2,' ',$name;
};
echo '</br>';
$message2 = "Hi";
$example2('Mary');

echo '</br>';
//调用闭包
function test_closure($name, Closure $clo){
    echo "Hello, {$name}".'</br>';
    $clo();
}
test_closure("Mary", function (){
    echo 'you are VIP';
});