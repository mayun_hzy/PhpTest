<?php
/**
 * Created by PhpStorm.
 * User: 59810
 * Date: 2019/7/22
 * Time: 08:51
 */
header('content-type:text/html;charset=utf-8');
echo "变量范围与生命周期";
echo '</br>';
echo '局部变量','</br>';
echo '1、当前页面中声明的普通变量，不能在函数或者类中起作用','</br>';
echo '2、当前页面中声明的普通变量，不能被传递到其他页面','</br>';
echo '3、在函数中声明的普通变量，在函数内部有效','</br>';
echo '4、在类中声明的属性，在类的内部有效','</br>';
echo '<hr>';

$a = 1;
function test_local() {
    echo $a;//输出无效
}
test_local();
echo '</br>';
echo '全局变量','</br>';
echo '1、对于php而言，可以这么说，在页面中生成的变量都是全局变量','</br>';
echo '，在整个页面中都是有效的。但是不能被函数或者类中的方法访问','</br>';
echo '2、如果想被函数或者类中的方法访问，我们就有了超全局变量','</br>';
echo '3、PHP自定义超全局变量：$_GET、$_POST、 $_COOKIE、 $_SERVER、 $_FILES、 $_ENV、 $_REQUEST以及$_SESSION','</br>';
echo '</br>';
$g_name = "Mary";
//第一种方法：global
function get_name() {
    global $g_name;//将变量$g_name全局化
    echo $g_name;
}
get_name();
echo '</br>';
//第二种方法: $GLOBALS['']
function get_name2() {
    echo $GLOBALS['g_name'];
}
get_name2();
echo '','</br>';
$v1 = 1;
$v2 = 2;
//函数内部的引用没有改变外部的值
function test_global() {
    global $v1, $v2;
    $v2 = &$v1;
}
test_global();
echo $v2;
echo '</br>';
//函数内部的引用改变外部的值
function test_global2() {
    //$GLOBALS['#'] 全局变量数组，引用了外部v1和v2的值
    $GLOBALS['v2'] = &$GLOBALS['v1'];
}
test_global2();
echo $v2;
echo '</br>';
echo '静态变量','</br>';
echo '<hr>';

echo '无static关键字';
function test_static() {
    $a = 0;
    echo $a++;
}
test_static();
test_static();
echo '</br>';
function test_static2() {
    static $b = 0;
    echo $b++;
}
echo '有static关键字';
test_static2();
test_static2();
echo '</br>';
function test_static3() {
   // static $b = sqrt(3);//所赋的值不能是函数表达式
    //echo $b++;
}
test_static3();
